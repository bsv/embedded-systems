#define is_digit(a) a>='0' && a<='9'

typedef struct{
  unsigned int direction_pin;
  unsigned int cur_direction;
  unsigned int speed_pin;
  unsigned int cur_speed;
}axis;

typedef struct{
    axis left;
    axis right;
    //soon here will be something else
}car;

typedef struct{
  unsigned int axis_num;
  unsigned int mode;
  unsigned int value;
}command_result;

unsigned char str_to_byte(char* str){ //length of string == 3
  unsigned int count = 0;
  unsigned int del = 100;
  
  while(*str && is_digit(*str)){
    count += (*str - '0')*del;
    del /= 10;
    str++;
    }
  return count;
  }

void parse_command(command_result* command, char* command_str){
  command->axis_num = command_str[0];
  command->mode = command_str[1];
  command->value = str_to_byte(command_str + 2);
  }

void change_car_state(car* this_car, char* command){
    
  command_result res = {0};

  parse_command(&res, command);
  
  axis* target_axis = 0;
  switch(res.axis_num){
    case '1':
      target_axis = &this_car->left; 
    break;
    case '2':
      target_axis = &this_car->right;
    break;
    }

  switch(res.mode){
    case 's':
      target_axis->cur_speed = res.value;
    break;
    case 'd':
       target_axis->cur_direction = res.value; 
    break;
    }

   // Serial.write(res.axis_num);
   // Serial.write(res.mode);
   // Serial.println(res.value);
    
}
void write_car_state(car* this_car){
  
  digitalWrite(this_car->left.direction_pin, this_car->left.cur_direction);
  digitalWrite(this_car->right.direction_pin, this_car->right.cur_direction);

  analogWrite(this_car->left.speed_pin, this_car->left.cur_speed);
  analogWrite(this_car->right.speed_pin, this_car->right.cur_speed);
  }

car this_car = {
  {7,0,5,0},
  {8,0,6,0}
 };

void setup() {
    pinMode(this_car.left.direction_pin, OUTPUT);
    pinMode(this_car.left.speed_pin , OUTPUT);
    pinMode(this_car.right.direction_pin, OUTPUT);
    pinMode(this_car.right.speed_pin, OUTPUT);

    //pinMode(11, OUTPUT);
    
    Serial.begin(9600);
}



void loop() {
  char buf[6] = {0};
  if(Serial.readBytes(buf, 5) == 5){
    Serial.println(buf);
    change_car_state(&this_car, buf);
    } 
   write_car_state(&this_car);
}
