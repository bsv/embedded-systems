const char none_mode = 0;
const char moisture_mode = 1;
const char timer_mode = 2;

const char moisture_key_out = 101;
const char mode_key_in = 1;
const char timer_duration_key_in = 2;
const char moisture_key_in = 3;
const char water_duration_key_in = 4;

void send_moisture() {
  int moisture = read_moisture();
  struct packet_data data;
  data.key = moisture_key_out;
  data.value[0] = (char) (moisture / 256);
  data.value[1] = (char) (moisture % 256); // big-endian
  send_packet(data);
}

void moisture_mode_cycle() {
  int moisture = read_moisture();
  if (moisture < moisture_border) {
    water(water_duration);
  }
}

void timer_mode_cycle() {
  if (millis() - timerMillis >= timer_duration) {
    timerMillis = millis();
    water(water_duration);
  }
}

void mode_cycle() {
  switch (current_mode) {
    case moisture_mode:
      send_moisture();
      moisture_mode_cycle();
      break;

    case timer_mode:
      send_moisture();
      timer_mode_cycle();
      break;
  }
}

void change_mode(char mode) {
  if (mode != current_mode) {
    current_mode = mode;
  }
}

void params_cycle() {
  struct packet_data data = read_packet();
  if (data.key != 0) {
    switch (data.key) {
      case mode_key_in:
        change_mode(data.value[1]);
        break;
      case moisture_key_in:
        moisture_border = data.value[0] * 256 + data.value[1];
        break;
      case timer_duration_key_in:
        timer_duration = (data.value[0] * 256 + data.value[1]) * 60000;
        break;
      case water_duration_key_in:
        water_duration = (data.value[0] * 256 + data.value[1]) * 1000;
        break;
    }
  }
}
