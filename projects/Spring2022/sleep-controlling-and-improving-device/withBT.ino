//кишки тюленя для презентаций
#include <Wire.h>
#include <SoftwareSerial.h>
#include <iarduino_RTC.h>
#include <math.h>
#include "TroykaRTC.h"
iarduino_RTC time(RTC_DS1307);

#define BETA 0.22f
#define LED_RED_PIN     9  //пины светодиодки
#define LED_GREEN_PIN   10
#define LED_BLUE_PIN    11
#define LEN_TIME 12
#define LEN_DATE 12
#define LEN_DOW 12

int brrr = 0; //мягкое мурчание
int koef = 4; //ускорение или замедление рассвета. на 2 часа 100
int hoursAlarm = 20; //получить с телефона и вычесть 2 часа
int minutesAlarm = 5;

int lightRed = 255; //первоначальные значения(темнота)
int lightGreen = 255;
int lightBlue = 255;

char incomingByte = 0;
char quick = 'q';
char slow = 'l';
char rrr = 'r';
char alarm = 'a';

char times[LEN_TIME]; //время
char date[LEN_DATE]; //дата
char weekDay[LEN_DOW]; //день недели

RTC clock;


void setup() {
  Serial.begin(9600);
  
  clock.begin();
  clock.set(__TIMESTAMP__);
  
  pinMode(LED_RED_PIN, LOW);
  pinMode(LED_GREEN_PIN, LOW);
  pinMode(LED_BLUE_PIN, LOW);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
}

void loop() {
  if (Serial.available() > 0) { //прием данных с тюленя
    incomingByte = Serial.read();
    //int i = incomingByte - 48;
    if (incomingByte == quick) {
      koef = 1;
      sunrise();
    }
    if (incomingByte == slow) {
      koef = 100;
      sunrise();
    }
    if (incomingByte == rrr) {
      murWake();
    }
  }
  wake();
}

void(* resetFunc) (void) = 0;//объявляем функцию reset с адресом 0

void wake() {
  if (Serial.available() > 0) {
    incomingByte = Serial.read();
    if (incomingByte == alarm) {
      brrr = 0;
      analogWrite(5, brrr);
      analogWrite(6, brrr);
      lightRed = 255;
      lightGreen = 255;
      lightBlue = 255;
      pinMode(LED_RED_PIN, LOW);
      pinMode(LED_GREEN_PIN, LOW);
      pinMode(LED_BLUE_PIN, LOW);
      resetFunc();
    }
  }
}

void sunrise() {
  while (lightRed > 60) { //нарастание красного
    analogWrite(LED_RED_PIN, lightRed);
    lightRed = lightRed - 1;
    delay(70 * koef); //70 для 2 часов
    wake();
  }

  while (lightGreen >= 170) { //переход в оранжевый
    analogWrite(LED_RED_PIN, lightRed);
    analogWrite(LED_GREEN_PIN, lightGreen);
    if (lightRed > 0)
      lightRed = lightRed - 2;
    lightGreen = lightGreen - 1;
    delay(169 * koef); //169 для 2 часов
    wake();
  }

  while (lightBlue >= 170) { //добавляем синеву
    analogWrite(LED_BLUE_PIN, lightBlue);
    lightBlue = lightBlue - 1;
    delay(169 * koef); //169 для 2 часов
    wake();
  }

  while (lightGreen > 0) { //уход в белый
    analogWrite(LED_GREEN_PIN, lightGreen);
    analogWrite(LED_BLUE_PIN, lightBlue);
    lightGreen = lightGreen - 2;
    lightBlue = lightBlue - 2;
    delay(85 * koef); //85 для 2 часов
    wake();
  }

  while (lightRed < 255) { //нарастание синего
    analogWrite(LED_GREEN_PIN, lightGreen);
    analogWrite(LED_RED_PIN, lightRed);
    if (lightGreen < 170)
      lightGreen = lightGreen + 1;
    lightRed = lightRed + 1;
    delay(56 * koef); //56 для 2 часов
    wake();
  }
  murWake();
}

void murWake() {
  while (brrr < 160) { //мурчит
    analogWrite(5, brrr);
    analogWrite(6, brrr);
    brrr++;
    wake();
    delay(300);
  }
}

void TIME() {
  clock.read();
  //Serial.println(times);
  //Serial.println(date);
  //Serial.println(weekDay);
  if ((hoursAlarm - 2 == clock.getHour()) && (minutesAlarm == clock.getMinute())) {
    sunrise();
    }
  clock.getTimeStamp(times, date, weekDay);
}
